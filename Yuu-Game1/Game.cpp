#include "Game.hpp"
#include "FileUtil.hpp"


string setting_file = "Setting.txt";


int main(int argc, char** argv){

	file_create(setting_file);

	int width = stoi(file_load(0, setting_file));
	int height = stoi(file_load(1, setting_file));
	SDL_Init(SDL_INIT_EVERYTHING);

	SDL_CreateWindow("Test",
		SDL_WINDOWPOS_UNDEFINED,
		SDL_WINDOWPOS_UNDEFINED,
		width, height,
		SDL_WINDOW_OPENGL);

	SDL_Delay(300000);

	SDL_Quit();

	return 0;
}
