#pragma once
#ifndef _FILEUTIL_H_
#define _FILEUTIL_H_

#include <fstream>
#include <iostream>
#include <string>
#include <cstdlib>
#include <typeinfo>

using namespace std;

	bool file_check(const string& filename);
	void file_create(string filename);
	string file_load(int line, string filename);

#endif
