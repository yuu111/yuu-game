#include "FileUtil.hpp"

bool file_check(const string& filename)
{
	ifstream ifs(filename);
	return ifs.is_open();
}

void file_create(std::string filename) {

		if (file_check(filename) == false) {
			ofstream outputfile(filename);
			outputfile << "800\n";
			outputfile << "600\n";
			outputfile.close();
		}
	}

std::string file_load(int line, std::string filename) {

		long i = 0;
		std::string str;
		bool flag = false;
		ifstream ifs(filename);

		if (ifs) {

			std::string line2;

			while (true) {

				getline(ifs, line2);

				if (i == line) {
					str = line2;
					flag = true;
				}
				i++;

				if (ifs.eof() || flag)
					break;
			}
		}
		return str;
	}
